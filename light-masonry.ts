interface SettingsInterface {
    // Required. Min height of a row
    ROW_MIN_HEIGHT: number,
    // Optional. Max width of a row
    ROW_MAX_WIDTH?: number | null,
    // Required. Spacing between items
    GUTTER: number,
    // Optional. Spacing between items for mobile
    MOBILE_GUTTER?: number,
    // Optional. Screens smaller than the defined will use the MOBILE_GUTTER. E.g. 992
    MOBILE_SCREEN_WIDTH?: number,
}

interface ItemInterface {
    element: HTMLElement,
    originalHeight: number,
    originalWidth: number,
    aspect: number,
    scale: number,
    width: number,
    height: number,
}

interface BucketInterface {
    items: ItemInterface[],
    width: number,
    height: number,
    isLast: boolean,
    scale: number,
}

const MASONRY_DATA = {
    data: {
        WIDTH: 'width',
        HEIGHT: 'height',
    },
}

// based on https://github.com/ionelmc/jquery-gp-gallery/blob/master/src/jquery-gp-gallery.js
class LightMasonry {
    private element: HTMLElement
    private settings: SettingsInterface
    private maxBucketWidth: number

    constructor(element: HTMLElement, settings: SettingsInterface) {

        this.element = element

        this.settings = settings
        this.maxBucketWidth = 0

        this.setup()
    }

    private getGutter() {
        if (!this.settings.MOBILE_SCREEN_WIDTH ||
            !this.settings.MOBILE_GUTTER) {
            return this.settings.GUTTER
        }

        let isMobile = window.innerWidth < this.settings.MOBILE_SCREEN_WIDTH

        return isMobile ?
            this.settings.MOBILE_GUTTER :
            this.settings.GUTTER
    }

    private getBucketWidth(items: ItemInterface[], extraWidth: number = 0): number {

        let width = 0

        if (items.length) {
            width = this.getGutter() * (items.length - 1)

            items.forEach((item) => {
                width += item.width
            })
        }

        width += extraWidth

        return width
    }

    private getBuckets(items: HTMLElement[]): BucketInterface[] {

        let buckets: BucketInterface[] = []

        // don't use this.element.clientWidth()
        // https://bugs.chromium.org/p/chromium/issues/detail?id=360889
        // https://bugzilla.mozilla.org/show_bug.cgi?id=825607
        // http://jsfiddle.net/7MMhB/
        let elementWidth = parseFloat(window.getComputedStyle(this.element).width)
        elementWidth = Math.floor(elementWidth)

        this.maxBucketWidth = this.settings.ROW_MAX_WIDTH || elementWidth
        let lastBucket: BucketInterface = {
            items: [],
            width: 0,
            height: 0,
            isLast: false,
            scale: 0,
        }

        items.forEach((item: HTMLElement) => {

            Object.assign(item.style, {
                width: 'auto',
                float: 'left',
                position: 'relative',
                clear: 'none',
                marginTop: 0,
                marginLeft: 0,
            })

            let originalHeight = parseInt(item.dataset[MASONRY_DATA.data.HEIGHT])
            let originalWidth = parseInt(item.dataset[MASONRY_DATA.data.WIDTH])
            let scale = this.settings.ROW_MIN_HEIGHT / originalHeight

            let itemData: ItemInterface = {
                element: item,
                originalHeight: originalHeight,
                originalWidth: originalWidth,
                aspect: originalWidth / originalHeight,
                scale: scale,
                // floor to avoid have decimal numbers
                width: Math.floor(originalWidth * scale),
                height: Math.floor(originalHeight * scale),
            }
            let newBucketWidth = this.getBucketWidth(lastBucket.items, itemData.width)

            if (newBucketWidth > this.maxBucketWidth) {

                buckets.push(lastBucket)
                lastBucket = {
                    items: [],
                    width: 0,
                    height: 0,
                    isLast: false,
                    scale: 0,
                }

            }

            lastBucket.items.push(itemData)
        })

        buckets.push(lastBucket)

        lastBucket.isLast = true

        return buckets
    }

    private updateItemStyles(buckets: BucketInterface[]): void {

        buckets.forEach((bucket, index) => {

            if (!bucket.isLast) {
                bucket.scale = (this.maxBucketWidth - (bucket.items.length - 1) * this.getGutter()) /
                    this.getBucketWidth(bucket.items)
            }

            let lastItem

            bucket.items.forEach((item, index2) => {
                if (bucket.scale) {
                    // floor to avoid have decimal numbers
                    item.width = Math.floor(item.width * bucket.scale)
                    item.height = Math.floor(item.height * bucket.scale)
                }

                lastItem = item

                Object.assign(item.element.style, {
                    height: item.height + 'px',
                    width: item.width + 'px',
                    marginTop: this.getGutter() + 'px',
                })

                if (index2 > 0) {
                    item.element.style.marginLeft = this.getGutter() + 'px'
                } else {
                    item.element.style.clear = 'left'
                }
            })

            // if !bucket.last &&
            if (lastItem) {
                lastItem.width = lastItem.width + this.maxBucketWidth - this.getBucketWidth(bucket.items)
                lastItem.element.style.width = lastItem.width + 'px'
            }
        })

    }

    private setup(): void {
        this.updateItems()
        this.element.dataset['masonry'] = 'masonry'

        window.addEventListener('resize', () => {
            this.updateItems()
        })
    }

    public updateItems(items: HTMLElement[] | null = null): void {

        items = items || <HTMLElement[]>Array.from(this.element.children)

        let buckets = this.getBuckets(items)
        this.updateItemStyles(buckets)
    }

    public addItems(items: HTMLElement[]): void {
        this.element.append(...items)

        this.updateItems(items)
    }
}
