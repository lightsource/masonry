# Light Masonry

## What is it?

TypeScript class for a masonry gallery, similar to existing masonry galleries, but it gives a flat bottom line. It's a
little modified TypeScript version of [jQuery masonry](https://github.com/ionelmc/jquery-gp-gallery).

### Markup

There is only one requirement to the markup - all children of the gallery element must have 'data-width' and 'data-height'
attributes. It's necessary to calculate a ratio for each. 

```
<div class="gallery">
<img src="x" data-width="" data-height="">
<img src="x" data-width="" data-height="">
</div>
```

It's allowed to have any markup for children, e.g.:

```
<div class="gallery">
<div data-width="" data-height=""><img src="x"><p>Some tooltip</p></div>
<div data-width="" data-height=""><img src="x"><p>Some tooltip</p></div>
</div>
```

### How to install

At this moment there is no NPM package for the library.

**TypeScript:**   
Download and include the 'light-masonry.ts' file within your TypeScript build.

**JavaScript:**   
Download and include the 'light-masonry.min.js' like an ordinary script (within the script tag).

### Initialization

```javascript
// galleryHtmlElement = parentElement of gallery items
let masonry = new LightMasonry(galleryHtmlElement, {
    ROW_MIN_HEIGHT: 180,
    GUTTER: 20,
    MOBILE_GUTTER: 10,
    MOBILE_SCREEN_WIDTH: 992,
});

// to add more items dynamically (e.g. after an ajax request)
// masonry.addItems(htmlItems)
```

### Available settings

```typescript
interface SettingsInterface {
    // Required. Min height of a row
    ROW_MIN_HEIGHT: number,
    // Required. Max height of a row
    ROW_MAX_HEIGHT: number,
    // Optional. Max width of a row
    ROW_MAX_WIDTH?: number | null,
    // Required. Spacing between items
    GUTTER: number,
    // Optional. Spacing between items for mobile
    MOBILE_GUTTER?: number,
    // Optional. Screens smaller than the defined will use the GUTTER_MOBILE. E.g. 992
    MOBILE_SCREEN_WIDTH?: number,
}
```
